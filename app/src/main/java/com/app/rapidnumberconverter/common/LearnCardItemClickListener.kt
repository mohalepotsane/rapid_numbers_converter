package com.app.rapidnumberconverter.common

interface LearnCardItemClickListener {
    fun onMoreButtonClick(item: LearnCardItem)
}