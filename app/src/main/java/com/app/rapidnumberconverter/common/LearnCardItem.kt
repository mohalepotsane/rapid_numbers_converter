package com.app.rapidnumberconverter.common

data class LearnCardItem(
    var title: String,
    var info: String,
    var url: String
)
