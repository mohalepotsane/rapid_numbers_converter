package com.app.rapidnumberconverter.utils

import com.app.rapidnumberconverter.common.LearnCardItem

val learnArticlez = listOf(
    LearnCardItem(
        "Binary numbers",
        "Lorem ipsum dolor sit amet. Et enim rerum id blanditiis facilis est eveniet voluptatem ea tempora animi ex dicta voluptatem qui galisum galisum ad cumque voluptatem. Sit quia nisi qui ducimus consectetur in consectetur amet? Aut similique beatae et accusamus ipsam eum aspernatur deleniti ad voluptatem sint qui maiores soluta qui velit ducimus quo officia sunt.",
        "https://www.mathsisfun.com/binary-number-system.html"
    ),
    LearnCardItem(
        "Hexadecimal numbers",
        "Lorem ipsum dolor sit amet. Et enim rerum id blanditiis facilis est eveniet voluptatem ea tempora animi ex dicta voluptatem qui galisum galisum ad cumque voluptatem. Sit quia nisi qui ducimus consectetur in consectetur amet? Aut similique beatae et accusamus ipsam eum aspernatur deleniti ad voluptatem sint qui maiores soluta qui velit ducimus quo officia sunt.",
        "https://www.mathsisfun.com/binary-number-system.html"
    ),
    LearnCardItem(
        "Binary numbers",
        "Lorem ipsum dolor sit amet. Et enim rerum id blanditiis facilis est eveniet voluptatem ea tempora animi ex dicta voluptatem qui galisum galisum ad cumque voluptatem. Sit quia nisi qui ducimus consectetur in consectetur amet? Aut similique beatae et accusamus ipsam eum aspernatur deleniti ad voluptatem sint qui maiores soluta qui velit ducimus quo officia sunt.",
        "https://www.mathsisfun.com/binary-number-system.html"
    ),
    LearnCardItem(
        "Octal numbers",
        "Lorem ipsum dolor sit amet. Et enim rerum id blanditiis facilis est eveniet voluptatem ea tempora animi ex dicta voluptatem qui galisum galisum ad cumque voluptatem. Sit quia nisi qui ducimus consectetur in consectetur amet? Aut similique beatae et accusamus ipsam eum aspernatur deleniti ad voluptatem sint qui maiores soluta qui velit ducimus quo officia sunt.",
        "https://www.mathsisfun.com/binary-number-system.html"
    )
)