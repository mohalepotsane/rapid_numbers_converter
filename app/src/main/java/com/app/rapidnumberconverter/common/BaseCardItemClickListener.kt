package com.app.rapidnumberconverter.common

interface BaseCardItemClickListener<T> {
    fun onCardClick(item: T)
}