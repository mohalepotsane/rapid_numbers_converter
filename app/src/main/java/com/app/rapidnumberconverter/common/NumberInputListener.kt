package com.app.rapidnumberconverter.common

interface NumberInputListener {
    fun onClearText()
    fun onCopyText()
    fun onPasteText()
}